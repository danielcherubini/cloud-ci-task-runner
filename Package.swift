import PackageDescription

let package = Package(
    name: "cloud-ci-task-runner",
    dependencies: [
        .Package(url: "https://github.com/vapor/vapor.git", majorVersion: 1, minor: 0)
    ]
)
