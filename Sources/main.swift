import Vapor
import Foundation

let drop = Droplet()
let launchPath = "/usr/bin/env"

func shell(launchPath: String, arguments: [String]) -> String
{

    let task = Process()
    task.launchPath = launchPath
    task.arguments = arguments

    let pipe = Pipe()
    task.standardOutput = pipe
    task.launch()

    let data = pipe.fileHandleForReading.readDataToEndOfFile()
    let output: String = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String

    return output
}

drop.get("/") { _ in

    let containers = shell(launchPath: launchPath, arguments: ["curl", "--unix-socket", "/var/run/docker.sock", "http:/containers/json"])

    return containers
}

drop.post("/",":task") { _ in


    return shell(launchPath: launchPath, arguments: ["curl", "--unix-socket", "/var/run/docker.sock", "http:/containers/json"])

}



drop.run()
